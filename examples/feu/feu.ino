#include <animlum.h>

Feu animation;
byte broche[]={2,3,4,5,6,7}; //ordre rouge1, rouge2, vert1, vert2, orange1, orange2

void setup() {
  animation.setup(broche, 0);//le 0 n'est pas nécessaire, mais si le montage est entre 5V et la broche, mettre 1 comme 2ème argument
}

void loop() {
   animation.update();
}
