#include <animlum.h>

Cheminee animation;
byte broche[]={2,3,4}; //ordre de la triple DEL : vert, rouge, bleu

void setup() {
  animation.setup(broche, 0);//le 0 n'est pas nécessaire, mais si le montage est entre 5V et la broche, mettre 1 comme 3ème argument
}

void loop() {
   animation.update();
}