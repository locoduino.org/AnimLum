#include <animlum.h>

Lampadaire animation;

void setup() {
  animation.setup(9, 0);//le 0 n'est pas nécessaire, mais si le montage est entre 5V et la broche, mettre 1 comme 2ème argument
}

void loop() {
   animation.update();
}