#include <animlum.h>

Enseigne animation;
const byte nombre=4;//changer le nombre si nécessaire
byte broche[]={2,3,4,5}; //mettre un nombre de broches = nombre

void setup() {
  animation.setup(nombre, broche, 0);//le 0 n'est pas nécessaire, mais si le montage est entre 5V et la broche, mettre 1 comme 3ème argument
}

void loop() {
   animation.update();
}