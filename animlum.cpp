/*Copyright 2015  G. Marty
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.*/

#include "animlum.h"

#include <Arduino.h>
//---------------------class led-----------------------------
Led::Led()
{
}

void Led::setup(byte apin, boolean amontageinverse)
{
  pin=apin;
  pinMode(pin, OUTPUT);
  if(amontageinverse==1){eOFF=HIGH; eON=LOW;}
  off();
}

void Led::on()
{
  digitalWrite(pin, eON);
}

void Led::off()
{
  digitalWrite(pin, eOFF);
}


//-----------------------class chenillard-------------------------------------

void Chenillard::off() 
{ 
  for (byte i = 0; i < nombre; i++) led[i].off(); 
}

void Chenillard::update() 
{ 
  if((millis()-temps)>200)
  {
    led[compteur].off();
    compteur++;
    if (compteur > (nombre-1)) compteur = 0;
    temps = millis(); 
    led[compteur].on();
  }
}

Chenillard::Chenillard(){
}
   
void Chenillard::setup(byte anombre, byte abroche[], boolean amontageinverse){
  nombre=anombre;
  led=new Led[nombre];
  for(byte i=0;i<nombre;i++){led[i].setup(abroche[i], amontageinverse);}  
  led[0].on();
}


//----------------------class enseigne---------------------

void Enseigne::rand()
{
   aleatoire=random(1,6);
}

Enseigne::Enseigne(){
}

void Enseigne::setup(byte anombre, byte abroche[], boolean amontageinverse){
  nombre=anombre;
  led=new Led[nombre];
  for(byte i=0;i<nombre;i++){led[i].setup(abroche[i], amontageinverse);}  
  off();
  led[0].on();
  rand();
}

void Enseigne::off()
{
  for(byte i=0;i<nombre;i++){led[i].off();}
}

void Enseigne::all()
{
  for(byte i=0;i<nombre;i++){led[i].on();}
}

void Enseigne::update()
{
  switch(aleatoire)//on sélectionne l'animation que le random a défini.
  {
    case 1:
    if ((millis() - temps) > 500 )
    {
      compteur++;
      temps=millis();
      enseigne1(compteur);  
    }
    break;
    
    case 2:
    if ((millis() - temps) > 150 )
    {
      temps=millis();
      for (byte i = 0; i < nombre; i++) { if (i == compteur) {led[i].on();} else {led[i].off();}}
      compteur++;
      if(compteur>nombre &&  b==1){ b=0; compteur=0; rand();}
      if(compteur>nombre){compteur=0; b=1;}
    }
    break;
    
    case 3:
    if ((millis() - temps) > 150 )
    {
      temps=millis();
      for (char i = (nombre-1); i > -1; i--) { if (i == ((nombre-1)-compteur)) {led[i].on();} else {led[i].off();}}
      compteur++;
      if(compteur>nombre &&  b==1){ b=0; compteur=0; rand();}
      if(compteur>nombre){compteur=0; b=1;}
    }
    break;
    
    case 4:
    if ((millis() - temps) > 150 )
    {
      temps=millis();
      enseigne2();
    }
    break;
    
    case 5:
    if ((millis() - temps) > 150 )
    {
      temps=millis();
      enseigne3();
    }
    break;
  }
}

void Enseigne::enseigne1(byte x)
{
  switch(x)
  {
    case 1:
    all();
    break;
    case 2://extinction des LED
    off();
    break;
    case 3:
    all();
    break;
    case 4://extinction des LED
    off();
    break;
    case 5:
    all();
    break;
    case 6://extinction des LED
    off();
    break;
    case 7:
    rand();
    compteur=0;
  }
}

void Enseigne::enseigne2()
{
  switch(b)
  {
    case 0:
      for (byte i = 0; i < nombre; i++) { if (i == compteur) {led[i].on();} else {led[i].off();}}
      compteur++;
      if(compteur>nombre){compteur=0; b=1;}
      break;
    case 1:
      for (char i = (nombre-1); i > -1; i--) { if (i == ((nombre-1)-compteur)) {led[i].on();} else {led[i].off();}}
      compteur++;
      if(compteur>nombre &&  c==1){ b=0; compteur=0; rand(); c=0;}
      if(compteur>nombre){compteur=0; b=0; c=1;}
      break;
  }
}

void Enseigne::enseigne3()
{
  if(c)
  {
    if(b)
    {
      led[(nombre-1)-compteur].off();
      compteur++;
      if(compteur>nombre){compteur=0; b=0;rand();c=0;}
    }
    else
    {
      led[(nombre-1)-compteur].on();
      compteur++;
      if(compteur>nombre){compteur=0; b=1;}
    }
  }
  else
  {
    if(b)
    {
      led[compteur].off();
      compteur++;
      if(compteur>nombre){compteur=0; b=0;c=1;}
    }
    else
    {
      led[compteur].on();
      compteur++;
      if(compteur>nombre){compteur=0; b=1;}
    }
  }
}

//-----------------------------class Feu------------------------------
Feu::Feu()
{
}

void Feu::setup(byte abroche[], boolean amontageinverse){
  for(byte i=0;i<6;i++){led[i].setup(abroche[i], amontageinverse);}  
  off();
  led[0].on();
  led[1].on();
}

void Feu::update()
{
  if((millis()-temps)>2000)
  {
    compteur++;
    temps = millis();
  }
  switch(compteur)
  {
  case 1:
  led[e].off();
  led[2+e].on();
  break;
  case 4:
  led[2+e].off();
  led[4+e].on();
  break;
  case 5:
  led[4+e].off();
  led[e].on();
  compteur = 0;
  e=!e;
  break;
  }
}

void Feu::off()
{
  for(byte i=0;i<6;i++){led[i].off();}
}

//--------------------class lampadaire------------------
Lampadaire::Lampadaire()
{
}

void Lampadaire::setup(byte apin, boolean amontageinverse){
  pin=apin;
  pinMode(pin, OUTPUT);
  montageinverse = amontageinverse;
  off();
  randomSeed(analogRead(0));
  aleatoire=random(30,100);
}

void Lampadaire::off()
{
  if(montageinverse){analogWrite(pin, 255);}
  else{analogWrite(pin, 0);}
  compteur=0;
}

void Lampadaire::update()
{
  if(compteur>60){
    if(montageinverse){analogWrite(pin, 0);}
    else{analogWrite(pin, 255);}
  }
  else if((millis()-temps)>aleatoire)
  {
    if(montageinverse){analogWrite(pin, 255-random(20,150));}
    else{analogWrite(pin, random(20,150));}
    aleatoire=random(30,100);
    compteur++;
    temps=millis();
  }
}
 
//----------------------class phare---------------------
Phare::Phare()
{
}

void Phare::setup(byte apin, boolean amontageinverse){
  pin=apin;
  pinMode(pin, OUTPUT);
  if(amontageinverse==1){
    for(byte i=0;i<33;i++){lum[i]=255-lum[i];}
  }
  off();
}


void Phare::off()
{
  analogWrite(pin,lum[0]);
}

void Phare::update()
{
  if((millis()-temps)>50)
  {
    analogWrite(pin,lum[compteur]);
    compteur++;
    temps=millis();
    if(compteur==32)
    {
      compteur=0;
    }
  }
}

//-----------------------class soudurearc------------------------------
void SoudureArc::off()
{
  led.off();
}

SoudureArc::SoudureArc() 
{
}

void SoudureArc::setup(byte apin, boolean amontageinverse){
  led.setup(apin, amontageinverse);
  randomSeed(analogRead(0));
  tempsflash=random(10,31);
  compte=random(10,21);
  tempspause=random(1500,7001);
}


void SoudureArc::update()
{
  if((compteur>compte)&&(boucle==0))
  {
    if((millis()-temps)>tempspause)
    {
      temps=millis();
      compte=random(10,21);
      tempspause=random(1500,7001);
      compteur=0;
    }
  }
  else
  {
    switch(boucle)
    {
      case 0:
      if((millis()-temps)>tempsflash)
      {
        boucle=1;
        led.on();
        temps=millis();
        dureeflash=random(10,101);
      }
      break;
      case 1:
      if((millis()-temps)>dureeflash)
      {
        boucle=0;
        led.off();
        temps=millis();
        tempsflash=random(10,31);
        compteur++;
      }
    }  
  }
}

//---------------------------class cheminée------------------------
Cheminee::Cheminee()
{
}

void Cheminee::setup(byte apin[], boolean amontageinverse){
  for(byte i=0;i<3;i++){pin[i]=apin[i];}
  montageinverse=amontageinverse;
  for(byte i=0;i<3;i++){pinMode(pin[i], OUTPUT);}
  off();
  randomSeed(analogRead(0));
  aleatoire=random(30,100);
}


void Cheminee::off()
{
  if(montageinverse){
    for(byte i=0;i<3;i++){digitalWrite(pin[i], HIGH);}
  }
  else{
    for(byte i=0;i<3;i++){digitalWrite(pin[i], LOW);}
  }
}

void Cheminee::update()
{
  if((millis()-temps)>aleatoire)
  {
    if(montageinverse){
      digitalWrite(pin[0], LOW);
      analogWrite(pin[1],random(20,80));
    }
    else{
      digitalWrite(pin[0], HIGH);
      analogWrite(pin[1],random(175,235));
    }
    temps=millis();
    aleatoire=random(30,100);
  }
} 