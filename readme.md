Copyright 2015  G. Marty  
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.

# Bibliothèque de jeux de lumières pour Arduino
avec l'aide de Locoduino.org


Bibliothèque pour Arduino avec animations lumineuses diverses dans le cadre d'un réseau de train miniature.

## Animations présentes :
* Phare
* Soudure à l'arc
* Cheminée
* Lampadaire
* Feu routier tricolore
* Enseigne de magasin
* Chenillard

Vous pourrez tout à fait en rajouter à la suite.

## Architecture générale
Chaque objet possède 3 méthodes :
* le constructeur permet de créer l'objet et n'a pas d'arguments.
* la méthode setup initialise l'objet (pinMode, état de base...) et accepte comme paramètres : 
1. le nombre de DEL dans les animations chenillard et enseigne dans les autres animations, cet argument n'est pas.
2. les numéros de broche : si animation avec une seule DEL, le numéro est à marquer, sinon les numéros de broche sont stockées dans un tableau.
3. un nombre booléen qui définit ou non l'inversion du circuit, la DEL pouvant être connecté de 2 façons soit broche et masse soit 5V et broche. De base ce paramètre est à 0 (il n'y a rien à mettre entre les parenthèse dans ce cas) pour une uilisation de DEL entre broche et GND. Si nous le mettons à 1, la DEL est entre 5V et la broche. Comme son nom l'indique à placer dans le void setup().
* la méthode update à placer dans le void loop(), elle met à jour l'objet pour faire défiler les animations en fonction du temps qui passe. Pas d'argument.

Il existe aussi la méthode off qui arrête l'animation.

## Comment brancher ?
Si animation comportant plusieurs DEL, l'ordre sur le terrain est important, le numéro des broches peut être complétement disparate pour, par exemple, garder les PWM nécessaires pour d'autres choses. Pour exemple, prenons un chenillard avec 6 DEL sur le terrain se suivant :

Numéro de DEL sur le terrain =>  Numéro de broche correspondant
* 1 => 2
* 2 => 4
* 3 => 7
* 4 => 8
* 5 => 12
* 6 => 13

Branchements faits sur un uno  

## Spécificités à chacun des objets

### Phare
accepte une seule broche qui doit être PWM.
Mot clé constructeur : Phare

### Soudure à l'arc
Animation à utiliser avec une DEL haute brillance. Le paramètre contient une seule broche non PWM.
Mot clé constructeur : SoudureArc

### Cheminée
Animation nécessitant 3 broches dont 1 au minimum PWM pour rationner les PWM qui va à la broche de la partie de DEL rouge, avec DEL RGB (anode ou cathode commune, le setup changera le sens si nécessaire). Il faudra peut être jouer avec les résistances pour obtenir un meilleur effet selon vos DEL RGB.
tableaubroche[]={vert, rouge, bleu}
Mot clé constructeur : Cheminee

### Lampadaire
Animation faisant grésiller une DEL. A utiliser avec une broche PWM
Mot clé constructeur : Lampadaire

### Chenillard
Animation pour les virages serrés par exemple. Nécessite x DEL avec broches non PWM.
Mot clé constructeur : Chenillard

### Enseigne de magasin
Animation pour magasins lumineux. Nécessite x DEL avec le même principe que le chenillard.
Mot clé constructeur : Enseigne

### Feu routier tricolore
Comme son nom l'indique, 6 broches non PWM sont nécessaires. L'ordre des numéros de broches dans le tableau est encore plus important que le chenillard :
tableauBroche[]={RougeFeu1, RougeFeu2, VertFeu1, VertFeu2, OrangeFeu1, OrangeFeu2};
Mot clé constructeur : Feu

## Exemples
Chaque animation a son exemple pour expliquer tout cela en pratique.