/*Copyright 2015  G. Marty
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses>.*/


#ifndef animlum.h
#define animlum.h

#include <Arduino.h>

//-------------------class led--------------------
class Led
{
  private:
    byte pin;
    boolean eOFF=LOW;
    boolean eON=HIGH;
  public:
    Led();
    void setup(byte apin, boolean amontageinverse=0);
    void on();
    void off();
};

//--------------------class chenillard----------------------------

class Chenillard 
{
  private: 
    Led *led; 
    byte nombre;
    unsigned long temps = 0; 
    byte compteur = 0; 
  public: 
    Chenillard();
    void setup(byte anombre, byte abroche[], boolean amontageinverse=0);
    void update();
    void off();
};

//-------------------class enseigne---------------------------------

class Enseigne
{
  private:
    Led *led;
    byte nombre;
    unsigned long temps = 0;
    byte aleatoire;
    byte compteur = 0;
    boolean b=0;
    boolean c=0;
    void all();
    void enseigne1(byte x);
    void enseigne2();
    void enseigne3();
    void rand();
  public:
    Enseigne();
    void setup(byte anombre, byte abroche[], boolean amontageinverse=0);
    void update();
    void off();
};

//------------------class feu--------------------------------------
class Feu
{
  private:
    Led led[6];
    unsigned long temps = 0;
    byte compteur = 0;
    boolean e=0;
  public:
    Feu();
    void setup(byte abroche[], boolean amontageinverse=0);
    void update();
    void off();
};

//------------------class lampadaire----------------------------
class Lampadaire
{
  private:
    byte pin;
    byte compteur=0;
    unsigned long temps=0;
    byte aleatoire;
    boolean montageinverse=0;
  public:
    Lampadaire();
    void setup(byte apin, boolean amontageinverse=0);
    void update();
    void off();
};

//---------------------class phare
class Phare
{
  private:
    byte pin;
    int lum[ 32 ] = { 0, 1, 1, 1, 2, 2, 2, 5, 5, 9, 14, 30, 50, 100, 140, 255, 140, 100, 50, 30, 14, 9, 5, 2, 1, 0, 0, 0, 0, 0, 0, 0 };
    byte compteur=0;
    unsigned long temps=0;
  public:
    Phare();
    void setup(byte apin, boolean amontageinverse=0);
    void update();
    void off();
};

//-----------------class soudurearc----------------------
class SoudureArc
{
  private:
    Led led;
    boolean boucle=0;
    byte dureeflash;
    unsigned long temps=0;
    byte compteur=0;
    byte tempsflash;
    byte compte;
    long tempspause;    
  public:
    void off();
    SoudureArc();
    void setup(byte apin, boolean amontageinverse=0);
    void update();    
}; 

//----------------------class cheminée-------------------
class Cheminee
{
  private:
    byte pin[3];
    boolean montageinverse;
    unsigned long temps=0;
    byte aleatoire;
  public:
    Cheminee();
    void setup(byte apin[], boolean amontageinverse=0);
    void update();
    void off();
};

#endif
